# petenv conda recipe

Home: https://gitlab.esss.lu.se/icshwi/petenv

Package MIT

Recipe license: BSD 3-Clause

Summary: conda recipe for conda package of petenv
